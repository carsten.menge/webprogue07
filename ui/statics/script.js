/*
    ---------------------------------------------------------------------------
    AJAX-Abfragen, alle 250ms werden vom UI-server die aktuellen Simulations-
    daten geholt.
    ---------------------------------------------------------------------------
*/
function ajaxControl(start){
    let indicator = document.getElementById("ajaxIndicator")
    let xmlHttpRequest = new XMLHttpRequest();
    
    if (start) {
        let blinkRed = false;
        ajaxTimer = setInterval(function(){
            
            // ----------------------------------------------------------------
            // AJAX-Request senden und Antwort zur Verarbeitung vorbereiten
            xmlHttpRequest.open("POST", "http://localhost:8080/getData");
            xmlHttpRequest.send();
            xmlHttpRequest.addEventListener("load", function(){
                if (xmlHttpRequest.responseText != "null") {
                    let trajectoriesJSON = JSON.parse(xmlHttpRequest.responseText);
                paint(trajectoriesJSON)
                }
            });
            // ----------------------------------------------------------------

            // ----------------------------------------------------------------
            // Indikator für AJAX-Requests
            if (blinkRed) {
                indicator.style.backgroundColor = "green";
                blinkRed = !blinkRed;
            } else {
                indicator.style.backgroundColor = "red";
                blinkRed = !blinkRed;
            }
            // ----------------------------------------------------------------
        },250);
    } else {
        clearInterval(ajaxTimer); 
        indicator.style.backgroundColor = "red";
    }
}

/*
    ---------------------------------------------------------------------------
    Simulation steuern
    ---------------------------------------------------------------------------
*/
function simControl(command) {

    xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open("POST", "http://localhost:8080/simControl");
    let data = new FormData();

    switch (command) {
        case "start":
            data.append("command", "start");
            break;
        case "stop":
            data.append("command", "stop");
            break;
        case "shutdown":
            data.append("command", "shutdown");
            break;
        case "rate":
            data.append("command", "rate");
            let rate = document.getElementById("rate").value;
            data.append("newRate", rate);
            break;
    }

    xmlHttpRequest.send(data);
}

/*
    ---------------------------------------------------------------------------
    Simulationsstand in canvas zeichnen
    ---------------------------------------------------------------------------
*/
function paint(trajectoriesJSON) {
    canvas = document.getElementById("canvas");
    ctx = canvas.getContext('2d');
    

    trajectoriesJSON.forEach(trajectory => {
        
        let color = trajectory.Color;

        trajectory.Points.forEach(point => {
            let x = point[0];
            let y = canvas.height - point[1];

            ctx.beginPath();
            ctx.arc(x, y, 5, 0, 2 * Math.PI);
            ctx.fillStyle = color;
            ctx.fill();
        });
        
    });
}


/*
    ---------------------------------------------------------------------------
    Sonstige Funktionen
    ---------------------------------------------------------------------------
*/
// Text für range-input-Elemente anzeigen
function updateText(spanID, value) {
    document.getElementById(spanID).innerHTML = value;
}
// Aktuelle Simulationsrate auslesen
function getSimRate() {
    xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open("GET", "http://localhost:8080/getSimRate");
    xmlHttpRequest.send();
    xmlHttpRequest.addEventListener("load", function(){
        document.getElementById("rate").value = xmlHttpRequest.responseText;
    });
}
/*
    ---------------------------------------------------------------------------
    Beim Öffnen
    ---------------------------------------------------------------------------
*/
window.addEventListener("load", function(){
    getSimRate();
})