package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	mgo "gopkg.in/mgo.v2"
)

/*
	---------------------------------------------------------------------------
	Globale Variablen
	---------------------------------------------------------------------------
*/

var pageFile = "statics/index.html"
var dbAddress = "localhost:27017"
var dbName = "schleuderDB"
var collectionSimData = "collectionSimData"
var collectionSimParameters = "collectionSimParameters"
var session *mgo.Session
var err error

/*
	---------------------------------------------------------------------------
	Typdefinitionen
	---------------------------------------------------------------------------
*/

type trajectory struct {
	Points [][]int `bson:"Points" json:"Points"`
	Color  string  `bson:"Color" json:"Color"`
	Speed  float64 `bson:"Speed" json:"Speed"`
	Angle  int     `bson:"Angle" json:"Angle"`
}

type simParameters struct {
	Running bool    `bson:"Running"`
	Paused  bool    `bson:"Paused"`
	Rate    float64 `bson:"Rate"`
}

/*
	---------------------------------------------------------------------------
	main()
	---------------------------------------------------------------------------
*/

func main() {

	// MongoDB session starten
	session, err = mgo.Dial(dbAddress)
	if err != nil {
		fmt.Printf("mgo: %s", err.Error())
	}
	defer session.Close()

	// Handler registrieren
	http.HandleFunc("/", pageHandler)
	http.HandleFunc("/getData", getData)
	http.HandleFunc("/addThrow", addThrow)
	http.HandleFunc("/simControl", simControl)
	http.HandleFunc("/getSimRate", getSimRate)

	// serve static files
	fs := http.FileServer(http.Dir("statics"))
	http.Handle("/statics/", http.StripPrefix("/statics/", fs))

	// Server starten
	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Printf("ListenAndServe: %s", err.Error())
	}
}

/*
    ---------------------------------------------------------------------------
	Handler
    ---------------------------------------------------------------------------
*/

// pageHandler stellt die Seite zur Verfügung
func pageHandler(w http.ResponseWriter, r *http.Request) {

	b, err := ioutil.ReadFile(pageFile)
	if err != nil {
		fmt.Printf("ioutil: %s", err.Error())
	}

	fmt.Fprintf(w, string(b))
}

// getData ruft die aktuellen Simulationsdaten aus der Datenbank ab und schickt
// sie als JSON zum Client
func getData(w http.ResponseWriter, r *http.Request) {

	db := session.DB(dbName)
	coll := db.C(collectionSimData)

	var allTrajectories []trajectory
	coll.Find(nil).All(&allTrajectories)

	allTrajectoriesJSON, err := json.Marshal(allTrajectories)
	if err != nil {
		fmt.Printf("json: %s\n", err.Error())
	}
	fmt.Fprint(w, string(allTrajectoriesJSON))
}

// addThrow fügt der Datenbank einen neuen Wurf hinzu, der berechnet werden soll
func addThrow(w http.ResponseWriter, r *http.Request) {

	// Strings zu int konvertieren, bei Fehler auf 0
	speed, err := strconv.Atoi(r.FormValue("speed"))
	if err != nil {
		fmt.Printf("strconv: %s\n", err.Error())
		speed = 0
	}
	angle, err := strconv.Atoi(r.FormValue("angle"))
	if err != nil {
		fmt.Printf("strconv: %s\n", err.Error())
		angle = 0
	}

	// In die Datenbank einfügen
	db := session.DB(dbName)
	coll := db.C(collectionSimData)

	trajectory := trajectory{}
	trajectory.Speed = float64(speed)
	trajectory.Angle = int(angle)
	trajectory.Color = ""

	err = coll.Insert(trajectory)
	if err != nil {
		fmt.Printf("Insert: %s", err.Error())
	}

	// HTTP Status Code 204: No Content, damit die Seite nicht verlassen wird
	w.WriteHeader(http.StatusNoContent)
}

// simControl steuert die Simulation und ihre Parameter
func simControl(w http.ResponseWriter, r *http.Request) {

	db := session.DB(dbName)
	coll := db.C(collectionSimParameters)

	// parameter auslesen
	parameter := simParameters{}
	coll.FindId(0).One(&parameter)

	command := r.FormValue("command")
	switch command {
	case "start":
		parameter.Paused = false
		parameter.Running = true
		break
	case "stop":
		parameter.Paused = true
		break
	case "shutdown":
		parameter.Running = false
		break
	case "rate":
		newRate, err := strconv.Atoi(r.FormValue("newRate"))
		if err != nil {
			fmt.Printf("strconv: %s\n", err.Error())
			fmt.Printf("Setting default value 500.")
			newRate = 500
		}
		parameter.Rate = float64(newRate)
		break
	}

	coll.UpdateId(0, parameter)

	// HTTP Status Code 204: No Content, damit die Seite nicht verlassen wird
	w.WriteHeader(http.StatusNoContent)
}

// getSimRate gibt die aktuelle Simulationsrate an den Client zurück
func getSimRate(w http.ResponseWriter, r *http.Request) {

	db := session.DB(dbName)
	coll := db.C(collectionSimParameters)

	parameter := simParameters{}
	coll.FindId(0).One(&parameter)

	fmt.Fprintf(w, strconv.Itoa(int(parameter.Rate)))
}
