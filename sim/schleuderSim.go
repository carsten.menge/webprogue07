package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

/*
	---------------------------------------------------------------------------
	Globale Variablen/Konstanten
	---------------------------------------------------------------------------
*/

var dbAddress = "localhost:27017"
var session *mgo.Session
var dbName = "schleuderDB"
var collectionSimData = "collectionSimData"
var collectionThrows = "collectionThrows"
var collectionSimParameters = "collectionSimParameters"
var parameter simParameters
var elapsedTime float64
var deltaT float64
var ticker *time.Ticker
var finishedChannel = make(chan bool)
var deltaTChangedChannel = make(chan bool)
var paused bool

const g float64 = 9.81

/*
	---------------------------------------------------------------------------
	Typdefinitionen
	---------------------------------------------------------------------------
*/

type trajectory struct {
	ID        bson.ObjectId `bson:"_id" json:"ID"`
	StartTime float64       `bson:"StartTime" json:"StartTime"`
	Points    [][]int       `bson:"Points" json:"Points"`
	Color     string        `bson:"Color" json:"Color"`
	Speed     float64       `bson:"Speed" json:"Speed"`
	Angle     int           `bson:"Angle" json:"Angle"`
}

type simParameters struct {
	Running bool    `bson:"Running"`
	Paused  bool    `bson:"Paused"`
	Rate    float64 `bson:"Rate"`
}

/*
	---------------------------------------------------------------------------
	main()
	---------------------------------------------------------------------------
*/

func main() {

	// MongoDB session starten
	session, _ = mgo.Dial(dbAddress)
	defer session.Close()

	// Parameter auslesen
	db := session.DB(dbName)
	coll := db.C(collectionSimParameters)
	coll.FindId(0).One(&parameter)
	parameter.Running = true
	if parameter.Rate == 0 {
		parameter.Rate = 500
	}
	coll.UpdateId(0, parameter)

	deltaT = parameter.Rate

	// Ticker setzen
	ticker = time.NewTicker(time.Millisecond * time.Duration(deltaT))

	// syncFunc starten
	go syncFunc()

	for {
		select {
		case <-finishedChannel:
			return
		case <-deltaTChangedChannel:
			ticker.Stop()

			db := session.DB(dbName)
			coll := db.C(collectionSimParameters)
			coll.FindId(0).One(&parameter)

			deltaT = parameter.Rate

			ticker = time.NewTicker(time.Millisecond * time.Duration(deltaT))

			// syncFunc neu starten:
			go syncFunc()
		}
	}

}

// syncFunc
func syncFunc() {

	for range ticker.C {

		// Parameter auslesen
		db := session.DB(dbName)
		coll := db.C(collectionSimParameters)
		coll.FindId(0).One(&parameter)
		paused = parameter.Paused

		// Wenn die Simulationsrate geändert wurde, channel benachrichtigen
		if parameter.Rate != deltaT {
			deltaTChangedChannel <- true
		}

		// Wenn Simulation gestoppt wurde, channel benachrichtigen
		if parameter.Running == false {
			finishedChannel <- true
		}

		if !paused {
			elapsedTime += deltaT
		}

		calculate(elapsedTime)
	}
}

// calculate berechnet die nächsten Koordinaten für alle trajectories
func calculate(t float64) {

	db := session.DB(dbName)
	coll := db.C(collectionSimData)

	// Aktuelle Wurfdaten einlesen
	var allTrajectories []trajectory
	err := coll.Find(nil).All(&allTrajectories)
	if err != nil {
		fmt.Printf("Find: %s\n", err.Error())
	}

	// Über alle Würfe iterieren
	for _, throw := range allTrajectories {

		if !paused {
			// Wenn der Wurf am Boden angekommen ist, entfernen
			if len(throw.Points) > 1 && throw.Points[len(throw.Points)-1][1] == 0 {
				coll.RemoveId(throw.ID)
				break
			}

			// Falls der Wurf noch keine Farbe zugewiesen hat, Farbe zuweisen
			// Damit ist er auch neu und bekommt eine Startzeit zugewiesen
			if throw.Color == "" {
				throw.Color = getRandomColor()
				throw.StartTime = t
			}

			relT := t - throw.StartTime

			// nächsten Punkt berechnen und dem Points-Slice hinzufügen
			v0 := throw.Speed
			alpha := float64(throw.Angle) * math.Pi / 180.0
			maxRange := int(v0 * v0 * math.Sin(2.0*alpha) / g)

			relT = relT / 1000.0

			x := int(v0 * relT * math.Cos(alpha))
			y := int(v0*relT*math.Sin(alpha) - ((g / 2.0) * relT * relT))

			// Wenn y < 0, also der Boden durchstoßen wurde, max Reichweite
			// einsetzen
			if y < 0 {
				x = maxRange
				y = 0
			}

			coords := []int{x, y}

			throw.Points = append(throw.Points, coords)
			coll.UpdateId(throw.ID, throw)
		}
	}
}

/*
	---------------------------------------------------------------------------
	Sonstige Funktionen
	---------------------------------------------------------------------------
*/

// getRandomColor gibt einen zufälligen Hex-Color-String zurück
func getRandomColor() string {

	var colorArray = []string{
		"AliceBlue", "AntiqueWhite", "Aqua", "Aquamarine", "Azure", "Beige",
		"Bisque", "Black", "BlanchedAlmond", "Blue", "BlueViolet", "Brown",
		"BurlyWood", "CadetBlue", "Chartreuse", "Chocolate", "Coral",
		"CornflowerBlue", "Cornsilk", "Crimson", "Cyan", "DarkBlue", "DarkCyan",
		"DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki",
		"DarkMagenta", "DarkOliveGreen", "Darkorange", "DarkOrchid", "DarkRed",
		"DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray",
		"DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue",
		"DimGray", "DimGrey", "DodgerBlue", "FireBrick", "FloralWhite",
		"ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite", "Gold", "GoldenRod",
		"Gray", "Grey", "Green", "GreenYellow", "HoneyDew", "HotPink", "IndianRed",
		"Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", "LawnGreen",
		"LemonChiffon", "LightBlue", "LightCoral", "LightCyan",
		"LightGoldenRodYellow", "LightGray", "LightGrey", "LightGreen",
		"LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue",
		"LightSlateGray", "LightSlateGrey", "LightSteelBlue", "LightYellow",
		"Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine",
		"MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen",
		"MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise",
		"MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin",
		"NavajoWhite", "Navy", "OldLace", "Olive", "OliveDrab", "Orange",
		"OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise",
		"PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum",
		"PowderBlue", "Purple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown",
		"Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver",
		"SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "Snow", "SpringGreen",
		"SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet",
		"Wheat", "White", "WhiteSmoke", "Yellow", "YellowGreen",
	}

	rand.Seed(time.Now().UTC().UnixNano())
	color := colorArray[rand.Intn(len(colorArray))]
	return color
}
